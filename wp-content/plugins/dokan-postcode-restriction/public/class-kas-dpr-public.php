<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://ideas.echopointer.com
 * @since      1.0.0
 *
 * @package    Kas_Dpr
 * @subpackage Kas_Dpr/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Kas_Dpr
 * @subpackage Kas_Dpr/public
 * @author     Syed Muhammad Shafiq <shafiq_shaheen@hotmail.com>
 */
class Kas_Dpr_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Kas_Dpr_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Kas_Dpr_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/kas-dpr-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Kas_Dpr_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Kas_Dpr_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/kas-dpr-public.js', array( 'jquery' ), $this->version, false );
		wp_localize_script( $this->plugin_name, 'kas_dpr_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
	}
	
	/**
	 * start session
	 *
	 * @since    1.0.0
	 */	
	public function kas_startSession(){
		if( !session_id() ){
    		session_start();
		} 
	}
	
	/**
	 * destroy session
	 *
	 * @since    1.0.0
	 */	
	public function kas_endSession(){
		session_destroy();   
		session_unset();  
	}

	/**
	 * Add extra fields vendors settings.
	 *
	 * @since    1.0.0
	 */
	public function zip_range_fields($current_user, $profile_info){

	
			$kas_postcode_range = isset( $profile_info['kas_postcode_range'] ) ? $profile_info['kas_postcode_range'] : '';
			$kas_postcodes = isset( $profile_info['kas_pcr_postcodes'] ) ? $profile_info['kas_pcr_postcodes'] : '';
			
			if($kas_postcode_range){
				$kas_postcode_range = explode('|',$kas_postcode_range);
			}else{
				$kas_postcode_range = array();
			}			
			
			$text_domain = $this->plugin_name;
			
			$args = array(
				'kas_postcode_range' => $kas_postcode_range,
				'kas_postcodes' => $kas_postcodes,
				'text_domain' => $text_domain,
			);
			
			kas_dpr_get_template('fields.php', $args);
	    
	}

	/**
	 * Save extra fields vendors settings.
	 *
	 * @since    1.0.0
	 */
	public function save_zip_range_fields($store_id){
		
        if(!empty($_POST['kas_pcr_from']) && !empty($_POST['kas_pcr_to'])){
        	$total_values = count($_POST['kas_pcr_from']);
        	$postcode = '';
        	for($i = 0; $i < $total_values; $i++){
        		$postcode .= sanitize_text_field($_POST['kas_pcr_from'][$i]). ','. sanitize_text_field($_POST['kas_pcr_to'][$i]) . ','. sanitize_text_field($_POST['kas_pcr_description'][$i]) . '|';
        	}
        	$dokan_settings['kas_postcode_range'] = sanitize_text_field($postcode);
        }
        if(!empty($_POST['kas_pcr_postcodes'])){
        	$dokan_settings['kas_pcr_postcodes'] = sanitize_text_field($_POST['kas_pcr_postcodes']);
        }		
		update_user_meta($store_id, 'kas_postcode_range',$dokan_settings['kas_postcode_range']);
		update_user_meta($store_id, 'kas_pcr_postcodes',$dokan_settings['kas_pcr_postcodes']);
		
		
		
		$existing_dokan_settings = get_user_meta( $store_id, 'dokan_profile_settings', true );
		$prev_dokan_settings     = ! empty( $existing_dokan_settings ) ? $existing_dokan_settings : array();
		$dokan_settings = array_merge( $prev_dokan_settings,$dokan_settings);
		update_user_meta( $store_id, 'dokan_profile_settings', $dokan_settings );
		
	}
	
	/**
	 * Restrict product in postcode range in product loop.
	 *
	 * @since    1.0.0
	 */
	public function add_content_after_addtocart_button_func( $link, $product ) { 

		$product_id = $product->get_id();
		$vendor_id = get_post($product_id)->post_author;
		
		if ( is_user_logged_in() ) {
		    $user_id = get_current_user_id();
		    $billing_postcode = get_user_meta( $user_id, 'billing_postcode', true );
		    $shipping_postcode = get_user_meta( $user_id, 'shipping_postcode', true );
		    if(empty($billing_postcode) && empty($shipping_postcode)){
		    	$link = wc_get_endpoint_url('edit-address', '', wc_get_page_permalink('myaccount'));
		    	$link = '<a href="'.esc_url($link).'" rel="nofollow" class="re_track_btn woo_loop_btn btn_offer_block add_to_cart_button product_type_simple">'.esc_html__('Set address to continue',$this->plugin_name).'</a>';
		    	echo $link; 
		    	return;
		    }else {
		    	$user_postcode = (isset($shipping_postcode)) ? $shipping_postcode : $billing_postcode;
		    }
		} else {
			if (empty($_SESSION['kas_postcode'])){
		    	$link = esc_html__('Kindly set zipcode',$this->plugin_name);
		    	echo wp_specialchars_decode($link);
		    	return;
			}else {
				$user_postcode = $_SESSION['kas_postcode'];
			}
		}
		
		$kas_postcode_range = get_user_meta($vendor_id, 'kas_postcode_range',true);
		$kas_postcodes = get_user_meta($vendor_id, 'kas_pcr_postcodes',true);
		
		if($kas_postcode_range){
			$kas_postcode_range = explode('|',$kas_postcode_range);
		}else{
			$kas_postcode_range = array();
		}		
		if($kas_postcodes){
			$kas_postcodes = explode(',',$kas_postcodes);
		}else{
			$kas_postcodes = array();
		}
		$exist = false;
		foreach($kas_postcode_range as $postcode_range){
			$inner_postcode = explode(',',$postcode_range);
		
			if(!empty($inner_postcode[0]) && !empty($inner_postcode[1])){
				$user_postcode = intval(preg_replace('/\D/', '', $user_postcode));
				if ( ($user_postcode >= $inner_postcode[0] && $user_postcode <= $inner_postcode[1] && !empty($inner_postcode)) || in_array($user_postcode,$kas_postcodes)) {
					$exist = true;
					break;
				}else{
					$change_postcode = '<br><a class="change_postcode add_to_cart_button product_type_simple">'.esc_html__('Change Postcode',$this->plugin_name).'</a>';
					$waring_text = esc_html__('Product not availabe in your area!',$this->plugin_name);
					$waring_text = $waring_text . $change_postcode;
				}
			}
		}
		
		if($exist){
			echo wp_specialchars_decode($link);
			return;
		}else {
			if(!empty($kas_postcode_range)){
				echo $waring_text;
			}else {
				echo wp_specialchars_decode($link);
			}
			return;
		}
	}

	/**
	 * show product availability on product details page.
	 *
	 * @since    1.0.0
	 */	
	public function single_product_custom_button( ) {
	
	    global $product;
	
	    // WooCommerce compatibility
	    if ( method_exists( $product, 'get_id' ) ) {
	        $product_id = $product->get_id();
	    } else {
	        $product_id = $product->id;
	    }

		$vendor_id = get_post($product_id)->post_author;
		
		if ( is_user_logged_in() ) {
			$user_id = get_current_user_id();
		    $billing_postcode = get_user_meta( $user_id, 'billing_postcode', true );
		    $shipping_postcode = get_user_meta( $user_id, 'shipping_postcode', true );
		    if(empty($billing_postcode) && empty($shipping_postcode)){
		    	$link = wc_get_endpoint_url('edit-address', '', wc_get_page_permalink('myaccount'));
		    	$link = '<a href="'.esc_url($link).'" rel="nofollow" class="add_to_cart_button product_type_simple">'.esc_html__('Set address to continue',$this->plugin_name).'</a>';
		    	echo wp_specialchars_decode($link); 
		    	return;
		    }else {
		    	$user_postcode = (isset($shipping_postcode)) ? $shipping_postcode : $billing_postcode;
		    }
		} else {
			if (empty($_SESSION['kas_postcode'])){
				
	    		remove_action( 'woocommerce_simple_add_to_cart', 'woocommerce_simple_add_to_cart', 30 );
		    	$link = esc_html__('Kindly set zipcode',$this->plugin_name);
		    	echo wp_specialchars_decode($link);
		    	return;
			}else {
				$user_postcode = $_SESSION['kas_postcode'];
			}
		}
		
		$kas_postcode_range = get_user_meta($vendor_id, 'kas_postcode_range',true);
		$kas_postcodes = get_user_meta($vendor_id, 'kas_pcr_postcodes',true);
		
		if($kas_postcode_range){
			$kas_postcode_range = explode('|',$kas_postcode_range);
		}else{
			$kas_postcode_range = array();
		}		
		if($kas_postcodes){
			$kas_postcodes = explode(',',$kas_postcodes);
		}else{
			$kas_postcodes = array();
		}
		$exist = false;
		//echo "<pre>"; print_r($kas_postcodes);die;
		foreach($kas_postcode_range as $postcode_range){
			$inner_postcode = explode(',',$postcode_range);
		
			if(!empty($inner_postcode[0]) && !empty($inner_postcode[1])){
				$user_postcode = intval(preg_replace('/\D/', '', $user_postcode));
				
				if ( ($user_postcode >= $inner_postcode[0] && $user_postcode <= $inner_postcode[1]) || in_array($user_postcode,$kas_postcodes)) {
					$exist = true;
					break;
				}else{
					$change_postcode = '<br><a class="change_postcode">'.esc_html__('Change PostCode',$this->plugin_name).'</a>';
					$waring_text = esc_html__('Product not availabe in your area!',$this->plugin_name);
					$waring_text = $waring_text . $change_postcode;
				}
			}
		}
		
		if($exist){
			echo wp_specialchars_decode($link);
			return;
		}else {
			if(!empty($kas_postcode_range)){
	    		remove_action( 'woocommerce_simple_add_to_cart', 'woocommerce_simple_add_to_cart', 30 );
				echo wp_specialchars_decode($waring_text);
			}else {
				echo wp_specialchars_decode($link);
			}
			return;
		}
	}	
	
	/**
	 * popup when site loaded.
	 *
	 * @since    1.0.0
	 */
	public function set_zipcode(){
		$args = array();
		$args['text_domain'] = $this->plugin_name;
		if ( is_user_logged_in() ) {
		    $user_id = get_current_user_id();
		    $user_postcode_billing = get_user_meta( $user_id, 'billing_postcode', true );
		    $user_postcode_shipping = get_user_meta( $user_id, 'shipping_postcode', true );
		    if(empty($user_postcode_billing) && empty($user_postcode_shipping)){
				kas_dpr_get_template('popup.php',$args);
		    }else{
		    	return;
		    }
		} else {
				if (empty($_SESSION['kas_postcode'])){
					kas_dpr_get_template('popup.php',$args);
				}
		}		
	}
	
	/**
	 * show postcode description on product details page.
	 *
	 * @since    1.0.0
	 */
	public function show_postcode_description(){
	    global $product;
	
	    // WooCommerce compatibility
	    if ( method_exists( $product, 'get_id' ) ) {
	        $product_id = $product->get_id();
	    } else {
	        $product_id = $product->id;
	    }
		$vendor_id = get_post($product_id)->post_author;	
	
		
		$kas_postcode_range = get_user_meta($vendor_id, 'kas_postcode_range',true);
		
		if($kas_postcode_range){
			$kas_postcode_range = explode('|',$kas_postcode_range);
		}else{
			$kas_postcode_range = array();
		}
		
		foreach($kas_postcode_range as $postcode_range){
			$inner_postcode = explode(',',$postcode_range);
			if(!empty($inner_postcode[2])){
				kas_dpr_get_template('postcode_description.php',array('description'=> $inner_postcode[2]));
			}
		}	
	}
	
	/**
	 * setup checkout postcode fileds.
	 *
	 * @since    1.0.0
	 */
	public function checkout_update_fields( $fields = array() ) {
		if ( !is_user_logged_in() || empty(get_user_meta( get_current_user_id(), 'billing_postcode', true ))) {
			$postcode = (isset($_SESSION['kas_postcode'])) ? $_SESSION['kas_postcode'] : '';
			$fields['billing']['billing_postcode']['default'] = $postcode;
			$fields['shipping']['shipping_postcode']['default'] = $postcode;
		}
		
		$fields['billing']['billing_postcode']['custom_attributes'] = array('readonly'=>'readonly');
		$fields['shipping']['shipping_postcode']['custom_attributes'] = array('readonly'=>'readonly');

	   // you must return the fields array 
	   return $fields;
	}
	
	/**
	 * set postcode in session.
	 *
	 * @since    1.0.0
	 */	
	public function set_postcode(){
		
		$postcode = $_POST['kas_postcode'];
		if ( is_user_logged_in() ) {
		    $user_id = get_current_user_id();
		    update_user_meta( $user_id, 'billing_postcode', sanitize_text_field($postcode) );
		    update_user_meta( $user_id, 'shipping_postcode', sanitize_text_field($postcode) );
		}else{
			$_SESSION['kas_postcode'] = $postcode;
		}
		wp_die();
	}
	
	/**
	 * change postcode.
	 *
	 * @since    1.0.0
	 */	
	public function change_postcode(){
		$args = array('text_domain' => $this->plugin_name);
		kas_dpr_get_template('popup.php', $args);
		wp_die();
	}
	

}
