(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note that this assume you're going to use jQuery, so it prepares
	 * the $ function reference to be used within the scope of this
	 * function.
	 *
	 * From here, you're able to define handlers for when the DOM is
	 * ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * Or when the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and so on.
	 *
	 * Remember that ideally, we should not attach any more than a single DOM-ready or window-load handler
	 * for any particular page. Though other scripts in WordPress core, other plugins, and other themes may
	 * be doing this, we should try to minimize doing that in our own work.
	 */
	
	jQuery( document ).ready(function() {
		  // Handler for .ready() called.
		jQuery( ".change_postcode" ).on( "click" ,function() {	
			var data = {
				'action': 'change_postcode',
			};

			// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
			jQuery.post(kas_dpr_ajax_object.ajax_url, data, function(response) {
				jQuery('body').prepend(response);
			});	
		});	

	});	

})( jQuery );

function submit_postcode(){
	'use strict';
	if(jQuery('#kas_postcode').val()){
		
		var data = {
			'action': 'set_postcode',
			'kas_postcode': jQuery('#kas_postcode').val()
		};

		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		jQuery.post(kas_dpr_ajax_object.ajax_url, data, function(response) {
			//alert(response);
			jQuery('#get_zipcode').css({"display": "none"});
			location.reload();
			
		});		
	}else {
		alert('Kindly enter valid postcode!');
	}
}

function close_model(){
	'use strict';
	jQuery('#get_zipcode').css({"display": "none"});
}

function add_postcode_range(){
	'use strict';
	var block = jQuery('.dokan_postcode_range_block');

	var html = '<div class="dokan-form-group">';
	html += '<label class="dokan-w2 dokan-control-label">From</label>';
	html += '<div class="dokan-w2">';
	html += '<input class="dokan-form-control"  type="text" name="kas_pcr_from[]" placeholder="Zip From">';
	html += '</div>';
	html += '<label class="dokan-w2 dokan-control-label">To</label>';
	html +=	'<div class="dokan-w2">';
	html +=	'<input class="dokan-form-control" type="text" name="kas_pcr_to[]" placeholder="Zip To">';
	html += '</div>';
	html +=	'</div>';
	html += '<div class="dokan-form-group">';
	html += '<label class="dokan-w3 dokan-control-label">Description</label>';
	html += '<div class="dokan-w5">';
	html += '<textarea name="kas_pcr_description[]" rows="5" cols="40" placeholder="Description for this range"></textarea>';
	html += '</div>';
	html += '</div>';

	block.append(html);

}