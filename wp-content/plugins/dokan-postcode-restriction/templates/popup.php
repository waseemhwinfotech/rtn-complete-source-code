<div id="get_zipcode" class="kas-modal">

  <!-- Modal content -->
  <div class="kas-modal-content">
    <span class="kas-close" onclick="close_model();">&times;</span>
  	<h2><?php esc_html_e('Insert Your Postcode',$text_domain);?></h2>
    <input type="hidden" name="action" value="set_postcode" />
    <input type="text" class="input-text" id="kas_postcode" name="kas_postcode" placeholder="<?php esc_attr_e('Enter valid postcode',$text_domain);?>" value="<?php echo sanitize_text_field($_SESSION['kas_postcode']);?>">
    <button onclick="submit_postcode();" class="button"><?php esc_html_e('Submit',$text_domain);?></button>
    
  </div>

</div>