	<div class="dokan-form-group">
		<label class="dokan-w3 dokan-control-label" for="setting_address">
			<?php esc_html_e('Allowed PostCode',$text_domain);?>
		</label>
		<div class="dokan-w5">
			<textarea name="kas_pcr_postcodes" rows="5" class="kas_select dokan-form-control" placeholder="<?php esc_attr_e('Add allowed postcodes comma saperated...',$text_domain);?>"><?php echo sanitize_text_field($kas_postcodes); ?></textarea>
			<br><span><?php esc_html_e('Add each postcode comma saperated',$text_domain);?></span>
		</div>
	</div>

                <div class="dokan_postcode_range_block">              
				<h2><?php esc_html_e('Zipcode Ranges',$text_domain);?></h2>
				<?php foreach($kas_postcode_range as $postcode_range){ 
					$inner_postcode = explode(',',$postcode_range);
					
					if(!empty($inner_postcode[0]) && !empty($inner_postcode[1])){
				?>
                    <div class="dokan-form-group">
                        <label class="dokan-w2 dokan-control-label"><?php esc_html_e('From',$text_domain);?></label>
                        <div class="dokan-w2">
                            <input class="dokan-form-control"  type="text" name="kas_pcr_from[]" value="<?php echo sanitize_text_field($inner_postcode[0]); ?>" placeholder="<?php esc_attr_e( 'Zip From', $text_domain ); ?>">
                        </div>  
                        <label class="dokan-w2 dokan-control-label"><?php esc_html_e('To',$text_domain);?></label>
                        <div class="dokan-w2">
                            <input class="dokan-form-control" type="text" name="kas_pcr_to[]" value="<?php echo sanitize_text_field($inner_postcode[1]); ?>" placeholder="<?php esc_attr_e( 'Zip To', $text_domain ); ?>">
                        </div>
                          
                    </div>
                    
                    <div class="dokan-form-group">

                        <label class="dokan-w3 dokan-control-label"><?php esc_html_e('Description',$text_domain);?></label>
                        <div class="dokan-w5">
							<textarea class="dokan-form-control" name="kas_pcr_description[]" rows="5" cols="40" placeholder="<?php esc_attr_e('Description for this range',$text_domain);?>"><?php echo sanitize_text_field($inner_postcode[2]); ?></textarea>
                        </div>                    
                    
                    </div>
				<?php 
					}
				}?>
                </div>
            

	<div class="dokan-form-group">
		<label class="dokan-w3 dokan-control-label">
			&nbsp;
		</label>
		<div class="dokan-w5">
			<a class="dokan-btn dokan-btn-danger dokan-btn-theme pr-btn" onclick="add_postcode_range();"><?php esc_html_e('Add postcode range...',$text_domain);?></a>
		</div>
	</div>