<?php

/**
 * The plugin bootstrap file
 *
 *
 * @link              http://ideas.echopointer.com
 * @since             1.0.0
 * @package           Kas_Dpr
 *
 * @wordpress-plugin
 * Plugin Name:       Dokan PostCode Restriction
 * Plugin URI:        https://ideas.echopointer.com
 * Description:       Set the range of Postcodes to restrict customers to purchase products.
 * Version:           1.0.0
 * Author:            Syed Muhammad Shafiq
 * Author URI:        https://codecanyon.net/user/kas5986/portfolio?ref=kas5986
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       kas-dpr
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'classes/class-kas-dpr.php';
require plugin_dir_path( __FILE__ ) . 'includes/functions.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_kas_dpr() {

	$plugin = new Kas_Dpr();
	$plugin->run();

}
run_kas_dpr();
