<?php

class pisol_efrs_pro_rules{
    function __construct($slug){
        $this->slug = $slug;
         /* this adds the condition in set of rules dropdown */
        add_filter("pi_".$this->slug."_condition", array($this, 'addRule'));
    }

    function addRule($rules){
        $rules['state'] = array(
            'name'=>__('State (Available in PRO Version)'),
            'group'=>'location_related',
            'condition'=>'state',
            'pro'=>true
        );
        $rules['postcode'] = array(
            'name'=>__('Postcode (Available in PRO Version)'),
            'group'=>'location_related',
            'condition'=>'postcode',
            'pro'=>true
        );
        $rules['zones'] = array(
            'name'=>__('Zone (Available in PRO Version)'),
            'group'=>'location_related',
            'condition'=>'zones',
            'pro'=>true
        );
        $rules['city'] = array(
            'name'=>__('City/Town (Available in PRO Version)'),
            'group'=>'location_related',
            'condition'=>'zones',
            'pro'=>true
        );
        $rules['variable_product'] = array(
            'name'=>__('Cart has variable product (Available in PRO Version)'),
            'group'=>'product_related',
            'condition'=>'variable_product',
            'pro'=>true
        );
        $rules['user_role'] = array(
            'name'=>__('User role (Available in PRO Version)'),
            'group'=>'user_related',
            'condition'=>'user_role',
            'pro'=>true
        );
        
        $rules['subtotal_after_discount'] = array(
            'name'=>__('Cart Subtotal (After Discount) (Available in PRO Version)'),
            'group'=>'cart_related',
            'condition'=>'subtotal_after_discount',
            'pro'=>true
        );

        $rules['cart_subtotal_after_discount_exclude_virtual'] = array(
            'name'=>__('Cart Subtotal (After Discount Excluding virtual product) (Available in PRO Version)'),
            'group'=>'cart_related',
            'condition'=>'subtotal_after_discount',
            'pro'=>true
        );

        $rules['weight'] = array(
            'name'=>__('Total Product Weight in Cart (Available in PRO Version)'),
            'group'=>'cart_related',
            'condition'=>'weight',
            'pro'=>true
        );

        $rules['width'] = array(
            'name'=>__('Max Width in cart (Available in PRO Version)'),
            'group'=>'cart_related',
            'condition'=>'width',
            'pro'=>true
        );

        $rules['height'] = array(
            'name'=>__('Max Height in cart (Available in PRO Version)'),
            'group'=>'cart_related',
            'condition'=>'height',
            'pro'=>true
        );

        $rules['length'] = array(
            'name'=>__('Max Length in cart (Available in PRO Version)'),
            'group'=>'cart_related',
            'condition'=>'length',
            'pro'=>true
        );

        $rules['coupon'] = array(
            'name'=>__('Coupons used (Available in PRO Version)'),
            'group'=>'cart_related',
            'condition'=>'coupon',
            'pro'=>true
        );

        $rules['shipping_class'] = array(
            'name'=>__('Shipping class (Available in PRO Version)'),
            'group'=>'cart_related',
            'condition'=>'shipping_class',
            'pro'=>true
        );

        $rules['shipping_class_total'] = array(
            'name'=>__('Shipping class total (Available in PRO Version)'),
            'group'=>'cart_related',
            'condition'=>'shipping_class_total',
            'pro'=>true
        );

        $rules['shipping_class_quantity'] = array(
            'name'=>__('Shipping class total quantity in cart (Available in PRO Version)'),
            'group'=>'cart_related',
            'condition'=>'shipping_class_quantity',
            'pro'=>true
        );

        $rules['payment_method'] = array(
            'name'=>__('Payment Method (Available in PRO Version)'),
            'group'=>'cart_related',
            'condition'=>'payment_method',
            'pro'=>true
        );

        $rules['day'] = array(
            'name'=>__('Day of the week (Available in PRO Version)'),
            'group'=>'other',
            'condition'=>'day',
            'pro'=>true
        );
        
        return $rules;
    }
}

new pisol_efrs_pro_rules(PI_EFRS_SELECTION_RULE_SLUG);