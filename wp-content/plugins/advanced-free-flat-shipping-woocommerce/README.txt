=== Conditional shipping & Advanced Flat rate shipping for WooCommerce ===
Contributors: jyotsnasingh520
Donate link: piwebsolution.com
Tags: conditional shipping, flexible shipping,  table rate, WooCommerce shipping, Shipping method, shipping,  Free shipping WooCommerce, Flat rate shipping, Advanced free shipping, Advanced flat shipping
Requires at least: 3.0.1
Tested up to: 5.7
sLicense: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

WooCommerce conditional shipping & WooCommerce Advanced Flat rate shipping plugin to Create Advanced Flat rate shipping or Free shipping method, with different advanced criteria to apply this shipping method

== Description ==

= Using WooCommerce conditional shipping =
With this WooCommerce Advanced Free shipping you can setup your own advanced rules to determine when a free shipping rate should be available for the customer.

= Using Advanced Flat Rate Shipping for WooCommerce =
With our Advanced Flat Rate Shipping Method for WooCommerce you can create your own rule. When this rules are satisfied the Advanced Flat rate shipping method will be available to the buyer

= You can apply specific custom shipping method based on the below condition or combination of the below conditions: =
Free rules
<ul>
 	<li><strong>Country-based shipping method</strong>: Assign a shipping method for the customer of the specific country</li>
	 <li><strong>Product-based shipping method</strong>: Assign different shipping method if the customer is purchasing a specific product, say if he is purchasing some very large item that needs different shipping method then you can do that using this rule, this will even work with the variable product</li>
 	<li><strong>Category based shipping method</strong>: Assign different shipping method if the customer is purchasing a product from a specific category, say if he is purchasing furniture category product you will need a shipping method that allows large-item shipping large items, whereas if he is buying from mobile category he can be shipped by normal shipping</li>
	<li><strong>Cart Sub Total based shipping method</strong>: If the Cart total reaches some specific value then you can offer him a different shipping method, E.g: if the user is buying 1000$ worth of product then you want to offer him fast shipping for free</li>
	<li><strong>Quantity based shipping method</strong>: If you want to offer a different shipping method based on the number of units purchased by the customer then you can do that using this rule</li>
	<li><strong>User-based shipping method</strong>: As the name suggests, you can offer some shipping method to some specific user on your site</li>
</ul>

Pro rules:
<ul>
 	<li><strong>State-based shipping method</strong>: Assign different shipping method as per the State/County of your customer</li>
 	<li><strong>Postcode/Zipcode based shipping method</strong>: If the user comes from a specific postcode, you can even assign rage of postcode like 9011...9090, this will assign the shipping method to all the customer whose postcode falls in 9011 to 9090</li>
	<li><strong>City/town</strong>: offer a shipping method based on the city/town selected by the customer. this does a string comparison to do the matching as city is a text field in the WooCommerce checkout process</li>
 	<li><strong>Zone-based shipping method</strong>: Assign different shipping method as per the Shipping zone of your customer</li>
 	<li><strong>Cart Sub Total (after discount) based shipping method</strong>: Some time the user add discount coupon so their subtotal reduces and if you want to consider those reduced total while deciding the shipping method you can do that using this rule (you have option to exclude virtual product from this total)</li>
 	<li><strong>Weight-based shipping method</strong>: If your want to offer different shipping method based on the total weight of the product in the order or cart then you can do this using this rule, it calculates the total weight of the product in the cart and then based on the set value in the rule it assigns a shipping method</li>
 	<li><strong>Product Width based shipping method</strong>: It finds the maximum width of the product in the cart and uses that as the width of the cart and compares with width value set by you in the rule and as per the logic set in the rule it assign a shipping method</li>
 	<li><strong>Product Height based shipping method</strong>: It's working is same as the Width working</li>
 	<li><strong>Product Length based shipping method</strong>: It's working is same as the Width working</li>
 	<li><strong>Coupon based shipping method</strong>: Using this you can show a shipping method if the customer has applied some specific coupon code</li>
 	<li><strong>Shipping class-based shipping method</strong>: Show a specific shipping method, if the user buys a product that belongs to some specific category of shipping class</li>
 	<li><strong>Payment method based shipping method</strong>: Show a specific shipping method, if the user buys select a specific payment gateway, E.g: If you have a shipping method that also collects a payment, then you can show that shipping method when user select cash on the delivery payment method</li>
 	<li><strong>User role-based shipping method</strong>: Using this you can assign a different shipping method as per the user role. E.g: you can offer a different shipping method to a registered customer and different shipping method to those who are doing a Guest checkout</li>
	<li><strong>User city based method</strong>: You can offer method based on user city, it is string comparison or city name</li>
	<li><strong>Shipping class total</strong> this rule applies when customer has purchased an x amount of product from specific shipping class</li> 
	<li><strong>Shipping class total quantity of product in cart</strong> this rule applies when customer has added x unit of product from a specific shipping class in his cart</li> 
	<li><strong>Shipping method on specific time of the day:</strong> Show a shipping method on specif time of the day, <br>E.g: Show a shipping method between 10am to 1pm only</li> 
	<li><strong>Shipping method based on Day of the week:</strong> Offer a shipping method based on the day of the week. <br>E.g: show a shipping method on Saturday and Sunday only</li> 
	<li><strong>Shipping method based on Selected delivery day:</strong> show a different shipping method based on the delivery day selected by the customer in Delivery date selector added by the plugin <a href="https://www.piwebsolution.com/product/order-delivery-date-time-and-pickup-locations-for-woocommerce/">Delivery date and time plugin</a><br> E.g: Show a shipping method when customer is option for sunday as delivery date</il>
</ul>

<ul>
<li><strong>Remove all other shipping methods</strong> when a particular shipping method is activated (PRO)</li>
<li><strong>Remove all other shipping methods of this plugin</strong>, when a particular shipping method is activated (PRO)</li>
<li><strong>Remove all other shipping methods of low priority of this plugin</strong>, when a particular shipping method is activated (PRO)</li> 
</ul>

= Custom charge in pro version = 
The Cost field allows you to charge a flat rate per item, a percentage based cost or a minimum fee.

Available placeholders:
[qty] – Number of products in the cart
[fee] – An additional fee. This fee has two optional arguments.
percent – A percentage based on total order cost.
min_fee – A minimum amount. Useful when using percentages.
max_fee – A maximum amount. Useful when using percentages.
Examples
10 + ( 2 * [qty] ) – A base shipping cost of $10 plus $2 for each item in the cart.
20 + [fee percent="10" min_fee="4"] – A base shipping cost of $20 plus 10% of the order total, which is at least $4.

similar To WooCommerce original flat rate shipping method 

== Additional charges (pro) ==

Using additional charges you can add/subtract charge from the base shipping charge, based on different conditions

https://www.youtube.com/watch?v=oGE6daMXrOk

This are the different conditions available:

* Cart Quantity: [Video](https://www.youtube.com/watch?v=0CTSrfgaKvc)
* Cart Weight: [Video](https://www.youtube.com/watch?v=TriQypJAgYI)
* Cart Subtotal: [Video](https://www.youtube.com/watch?v=30tS78nMk40)
* Product Quantity: [Video](https://www.youtube.com/watch?v=lD7gm9PHkvE)
* Category Quantity: [Video](https://www.youtube.com/watch?v=6S1eVLuR6b8)
* Shipping Class Quantity: [Video](https://www.youtube.com/watch?v=DK04pdaB4u0)
* Product Weight: [Video](https://www.youtube.com/watch?v=aOjKK5LfR04)
* Category Weight: [Video](https://www.youtube.com/watch?v=gyhR2OvUDgw)
* Shipping Class Weight: [Video](https://www.youtube.com/watch?v=qIZM7VUUy1c)
* Product Subtotal: [Video](https://www.youtube.com/watch?v=sFdiwsoWvBw)
* Category Subtotal: [Video](https://www.youtube.com/watch?v=XPNsq5U6FHA)
* Shipping Class Subtotal: [Video](https://www.youtube.com/watch?v=GFuvQlEiELE)


== Frequently Asked Questions ==
= I want to offer flat rate shipping for specific county =
Yes you can do that using our country based rule

= I want to offer flat rate shipping for specific country when there subtotal exceeds certain amount =
Yes you can do that by creating a shipping method and adding rule of country and subtotal in it, so when the subtotal will be grater then specified amount and the country selected is as per your rule it will apply this flat rate shipping

= I want to set postal code range in the condition =
In the pro version you can set range of the postal code in the shipping method rule

= I want to remove other shipping method when a particular shipping method is applied =
You can do that in the pro version 

= I want to charge customer a different shipping rate on Weekend then weekdays =
In the pro version we have a rule that allow you to offer a different shipping method based on the day of the week 

= I want to show a shipping method only on weekend =
Yes there is rule in the pro version that allows you to show shipping method based on the order placement day of the week

= I want to show a shipping method during the specific time of the day =
Yes you can do that in the pro version, there is option to specify the time range and that shipping method will be shown during that time of the day only

= I want to show a shipping method based on the Custom delivery date selected by the customer =
Yes for this you will need our extra plugin <a href="https://www.piwebsolution.com/product/order-delivery-date-time-and-pickup-locations-for-woocommerce/">Delivery date and time plugin</a> that allow customer to select delivery date they want, and then the pro version of this plugin will allow you to create rule based on the day user selected date falls, E.g: if you want to offer free shipping on Sundays, so when the customer will select a delivery date that is on Sunday then that shipping method will be shown to him