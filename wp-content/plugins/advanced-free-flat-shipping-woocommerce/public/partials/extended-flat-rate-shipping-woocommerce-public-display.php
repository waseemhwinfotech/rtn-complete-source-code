<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       piwebsolution.com
 * @since      1.0.0
 *
 * @package    Extended_Flat_Rate_Shipping_Woocommerce
 * @subpackage Extended_Flat_Rate_Shipping_Woocommerce/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
