<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Efrs_Shipping_Method' ) ) {
	class Efrs_Shipping_Method extends WC_Shipping_Method {
		public function __construct() {
			$get_id     = filter_input( INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT );
			$post_title = isset( $get_id ) ? get_the_title( $get_id ) : '';
			
			$method_id    = isset( $get_id ) && ! empty( $get_id ) ? $get_id : 'pisol_extended_flat_shipping';
			$method_title = ! empty( $post_title ) ? $post_title : 'Extended Flat Rate Shipping';
			
			$this->id                 = $method_id;
			$this->method_title       = __( $method_title, 'advanced-free-flat-shipping-woocommerce' ); 
			$this->enabled            = "yes"; 
			$this->title              = __('Extended Flat Rate Shipping', 'advanced-free-flat-shipping-woocommerce'); 

			$this->init();

			add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
		}

		function init() {
			$this->init_form_fields(); 
			$this->init_settings(); 
			
		}

		public function calculate_shipping( $package=array() ) {
			$applicable_shipping_methods = $this->matchedShippingMethods($package);
			foreach($applicable_shipping_methods as $method){
				$cost = get_post_meta($method->ID, 'pi_cost', true);
				$extra_cost_type = get_post_meta($method->ID, 'pi_extra_cost_calc_type', true);
				$taxable = get_post_meta($method->ID, 'pi_is_taxable', true) === 'no' ? false : "";
				
				if($cost != "" & $cost > 0){
					$found_shipping_classes = $this->findShippingClasses( $package );
					$extra_cost = get_post_meta($method->ID, 'shipping_extra_cost', true);

					$cost = $cost + $this->extraCostAddition($found_shipping_classes, $extra_cost, $extra_cost_type);
				}

				$cost = apply_filters('pi_efrs_add_additional_charges', $cost, $method->ID, $package);

				$rate = array(
					'id' => 'pisol_extended_flat_shipping' . ':' . $method->ID,
					'label' => $method->post_title,
					'cost' => $cost,
					'taxes' => $taxable 
				);

				// Register the rate
				$this->add_rate( $rate );
				do_action( 'woocommerce_' . $this->id . '_shipping_add_rate', $this,$rate, $package );
			}
		}

		function extraCostAddition($found_shipping_classes, $shipping_method_extra_costs, $extra_cost_type){
			$extra_costs = array();
			if(is_array($found_shipping_classes) && count($found_shipping_classes) > 0){
				foreach($found_shipping_classes as $key => $found_shipping_class){
					if( !empty($shipping_method_extra_costs)  && is_array($shipping_method_extra_costs) ){
					foreach($shipping_method_extra_costs as $id => $cost){
						if($id === $key){
							if($cost != "" && is_numeric($cost)){
								$extra_costs[] = (float)$cost;
							}
						}
					}
					}
				}
			}
			if(count($extra_costs) > 0){
				if($extra_cost_type == 'class'){
					return array_sum($extra_costs);
				}else{
					return max($extra_costs);
				}
			}else{
				return 0;
			}
		}

		function matchedShippingMethods($package){
			$matched_methods = array();
			$args         = array(
				'post_type'      => 'pi_shipping_method',
				'posts_per_page' => - 1
			);
			$all_methods        = get_posts( $args );
			foreach ( $all_methods as $method ) {

				$is_match = $this->matchConditions( $method, $package );

				if ( $is_match === true ) {
					$matched_methods[] = $method;
				}
			}

			return $matched_methods;
		}

		public function matchConditions( $method = array(), $package = array() ) {

			if ( empty( $method ) ) {
				return false;
			}

			if ( ! empty( $method ) ) {
				$method_eval_obj = new Pisol_efrs_method_evaluation($method, $package);
				$final_condition_match = $method_eval_obj->finalResult();

				if ( $final_condition_match ) {
					return true;
				}
			}

			return false;
		}

		public function findShippingClasses( $package ) {
			$found_shipping_classes = array();

			foreach ( $package['contents'] as $item_id => $values ) {
				if ( $values['data']->needs_shipping() ) {
					$found_class = $values['data']->get_shipping_class();
					$found_id = $values['data']->get_shipping_class_id(); 
					if ( ! empty($found_class) && ! empty($found_id) ) {
						$found_shipping_classes[ $found_id ][ $item_id ] = $values;
					}
				}
			}

			return $found_shipping_classes;
		}
    }
}
