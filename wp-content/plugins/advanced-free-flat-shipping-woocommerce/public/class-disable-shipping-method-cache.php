<?php
class pisol_efrs_disable_shipping_method_cache{
    function __construct(){
        add_action('wp_loaded', array($this, 'clear_wc_shipping_rates_cache'));
    }

    function clear_wc_shipping_rates_cache(){
        if(!isset(WC()->cart) || !function_exists('WC')) return;
        
        if ( !WC()->customer ) return; // this allows it to work with woocommerce point of sales plugin
        
        $packages = WC()->cart->get_shipping_packages();

        foreach ($packages as $key => $value) {
            $shipping_session = "shipping_for_package_$key";

            unset(WC()->session->$shipping_session);
        }
        
   }
}

new pisol_efrs_disable_shipping_method_cache();