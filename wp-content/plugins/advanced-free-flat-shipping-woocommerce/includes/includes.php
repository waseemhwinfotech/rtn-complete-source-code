<?php
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/pisol.class.form.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/pisol.class.promotion.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/help.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-extended-flat-rate-shipping-woocommerce-menu.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-extended-flat-rate-shipping-woocommerce-add-shipping-method.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-extended-flat-rate-shipping-woocommerce-list-shipping-method.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/selection_rules/includes.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/additional-charges/includes.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-disable-shipping-method-cache.php';


