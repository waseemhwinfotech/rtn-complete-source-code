<?php
/*
* Plugin Name: WooCommerce Buy Again
* Description: The plugin for WooCommerce Buy Again.
* Plugin URI: https://wpmajesty.com/
* Version: 1.0.0
* Author: WPMajesty
* Author URI: https://wpmajesty.com/
* Support: https://www.wpmajesty.com/
* License: GPL-2.0+
* License URI: http://www.gnu.org/licenses/gpl-2.0.txt
* Text Domain: wc-wba
* Domain Path: /languages/
*/
if ( !defined( 'ABSPATH' ) ) exit();
if ( !defined( 'WP_BA_URL' ) ) {
	define( 'WP_BA_URL' , plugin_dir_url( __FILE__ ) );
}
if ( !defined('WP_BA_DIR') ) {
	define( 'WP_BA_DIR' , plugin_dir_path( __FILE__ ) );
}
if ( !class_exists( 'WBA_Buy_Again' ) ) {
	class WBA_Buy_Again {
		public function __construct() {
			if ( in_array('woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option('active_plugins')) ) ) {
				$this->wba_init_plugin_files();
			}else{
				add_action( 'admin_notices', array( $this, 'wba_admin_notice') );
			}
		}
		public function wba_init_plugin_files() {
			if ( function_exists( 'load_plugin_textdomain' ) ) {
				load_plugin_textdomain( 'wc-wba', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
			}
			require_once WP_BA_DIR.'include/common.php';
			require_once WP_BA_DIR.'include/enqueue-scripts.php';
			if( is_admin() ) {
				require_once WP_BA_DIR.'include/backend/admin.php';
			}
			else {
				if ( get_option('wba_buy_again_enable_module') == 'yes' ) {
					require_once WP_BA_DIR.'include/frontend/frontend.php';
				}
			}
		}
		public function wba_admin_notice() {
			global $pagenow;
	        if ( $pagenow === 'plugins.php' ) {
	            $class = esc_attr( 'notice notice-error is-dismissible' );
	            $message = esc_html__('WooCommerce Buy Again plugin needs WooCommerce to be installed and active.', 'wc-wba');
	            printf('<div class="%1$s"><p>%2$s</p></div>', $class, $message);
		    }
		}
	}
	new WBA_Buy_Again();
}