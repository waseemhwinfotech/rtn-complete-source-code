<?php
if ( !defined('ABSPATH') ) {
    exit;
}
if ( !class_exists('WBA_Buy_Again_Common') ) {
	class WBA_Buy_Again_Common {
		public function __construct() {
			add_action('wp_ajax_wba_add_to_cart_item', array($this, 'wba_add_to_cart_item'));
            add_action('wp_ajax_nopriv_wba_add_to_cart_item', array($this, 'wba_add_to_cart_item'));
            add_action('wp_ajax_wba_check_sold_item', array($this, 'wba_check_sold_item'));
			add_action('wp_ajax_nopriv_wba_check_sold_item', array($this, 'wba_check_sold_item'));
        }
		public function wba_add_to_cart_item() {
			if ( empty($_POST['product_id']) ) {
				wp_send_json_error();
			}
			$product_id = absint( wc_clean($_POST['product_id']) );
			$qty = !empty($_POST['qty']) ? absint( wc_clean($_POST['qty']) ) : 1;
			WC()->cart->add_to_cart( $product_id, $qty );
            wp_send_json_success();
        }
        public function wba_check_sold_item() {
            if ( empty($_POST['variation_id']) ) {
                wp_send_json_error();
            }
            $variation_id = wc_clean($_POST['variation_id']);
            $product_ids = array();
            $order_ids = get_posts(array(
                'numberposts' => -1,
                'meta_key'    => '_customer_user',
                'meta_value'  => wc_clean($_POST['user_id']),
                'post_type'   => wc_get_order_types(),
                'post_status' => array_keys(wc_get_order_statuses()),
                'fields'      => 'ids',
            ));
            if ( !empty($order_ids) ) {
                foreach( $order_ids as $order_id ) {
                    $order = wc_get_order($order_id);
                    if( !empty($order) ) {
                        foreach ( $order->get_items() as $item_id => $item ) {
                            $product_id = $item->get_variation_id();
                            array_push($product_ids, $product_id);
                        }
                    }
                }
            }
            if ( in_array($variation_id, $product_ids) ) {
                wp_send_json_success();
            }else{
                wp_send_json_error();
            }
        }
    }
    new WBA_Buy_Again_Common();
}