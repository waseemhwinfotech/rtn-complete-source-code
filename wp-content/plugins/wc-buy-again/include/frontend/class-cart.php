<?php
if ( !defined('ABSPATH') ) {
    exit;
}
if ( !class_exists ( 'WBA_Add_to_Cart' ) ) {
	class WBA_Add_to_Cart {
		public function __construct() {
			add_action( 'wp_loaded', array($this, 'wba_bulk_add_to_cart') );
		}
		public function wba_bulk_add_to_cart() {
			if ( isset($_POST['wba_order_again_submit']) ) {
                $product_ids = wc_clean( $_POST['wba_order_products'] );
                $product_ids = explode(',', $product_ids);
                if ( !empty($product_ids) ) {
                    foreach ( $product_ids as $product_id ) {
                        WC()->cart->add_to_cart( $product_id, 1);
                    }
                }
            }
		}
    }
    new WBA_Add_to_Cart();
}