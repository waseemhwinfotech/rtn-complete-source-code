<?php
if ( !defined('ABSPATH') ) {
    exit;
}
if ( !class_exists( 'WBA_Buy_Again_Frontend' ) ) {
	class WBA_Buy_Again_Frontend {
		public function __construct() {
            require_once WP_BA_DIR.'include/frontend/class-cart.php';
            add_filter( 'woocommerce_account_menu_items', array($this, 'wba_myaccount_page_endpoints') );
            add_action('init', array($this, 'wba_buy_again_products'));
            add_action('woocommerce_before_single_product', array($this, 'wba_product_notice'));
            add_filter( 'woocommerce_account_orders_columns', array($this, 'wba_woocommerce_account_orders_columns'), 10, 1 );
            add_action('woocommerce_my_account_my_orders_column_wba-again', array($this, 'wba_woocommerce_account_orders_columns_data'));
            add_action( 'woocommerce_order_details_after_order_table', array($this, 'wba_woocommerce_account_orders_columns_data') );
        }
        function wba_woocommerce_account_orders_columns_data( $order ) {
            $product_ids = array();
            foreach($order->get_items() as $item){
                $product_id = !empty($item->get_variation_id()) ? $item->get_variation_id() : $item->get_product_id();
                array_push($product_ids, $product_id);
            }
            $product_ids = implode(',', $product_ids);
            $label = !empty(get_option('wba_buy_again_order_button_label')) ? get_option('wba_buy_again_order_button_label') : esc_html__('Buy Again', 'wc-wba');
            echo '<form method="post">
                <input type="hidden" name="wba_order_products" value="' . esc_attr($product_ids) . '" />
                <button type="submit" name="wba_order_again_submit">'. esc_attr($label) . '</button>
            </form>';
        }
        function wba_woocommerce_account_orders_columns( $columns ) {
            $columns['wba-again'] = esc_html__('Add to cart', 'wc-wba');
            return $columns;
        }
        public function wba_product_notice() {
            $product_ids = array();
            if( is_product() ) {
                $order_ids = get_posts(array(
                    'numberposts' => -1,
                    'meta_key'    => '_customer_user',
                    'meta_value'  => get_current_user_id(),
                    'post_type'   => wc_get_order_types(),
                    'post_status' => array_keys(wc_get_order_statuses()),
                    'fields'      => 'ids',
                ));
                if( !empty($order_ids) ) {
                    foreach( $order_ids as $order_id ) {
                        $order = wc_get_order($order_id);
                         if( !empty($order) ) {
                            foreach ( $order->get_items() as $item_id => $item ) {
                               $product_id = $item->get_product_id();
                               $variation_id = $item->get_variation_id();
                                if( $variation_id != 0 ) {
                                    $product_id = $variation_id;
                                }
                                array_push($product_ids, $product_id);
                            }
                        }
                    }
                }
                if( in_array(get_the_ID(), $product_ids) ) {
                    $description = !empty(get_option('wba_buy_again_information')) ? get_option('wba_buy_again_information') : esc_html__('You have bought this product already. ', 'wc-wba');
                    wc_add_notice( sprintf('%s %s', $description , ' <a href="'.esc_url(home_url('my-account/buy_again')).'">'.esc_html__(' View','wc-wba'), '</a>'), 'notice' );
                }
            }
        }
        public function wba_buy_again_products() {
            add_rewrite_endpoint( 'buy_again', EP_ROOT | EP_PAGES );
            add_action( 'woocommerce_account_buy_again_endpoint', function(){
                 $order_ids = get_posts(array(
                    'numberposts' => -1,
                    'meta_key'    => '_customer_user',
                    'meta_value'  => get_current_user_id(),
                    'post_type'   => wc_get_order_types(),
                    'post_status' => array_keys(wc_get_order_statuses()),
                    'fields'      => 'ids',
                ));
                if( !empty($order_ids) ) {
                    echo '<h3>'.esc_html__('Buy Again', 'wc-wba').'</h3>
                        <table>
                            <tr><td>'.esc_html__('Thumbnail', 'wc-wba').'</td><td>'.esc_html__('Title', 'wc-wba').'</td><td>'.esc_html__('Price ', 'wc-wba').'</td><td>'.esc_html__('Quantity', 'wc-wba').'</td><td>'.esc_html__('Action', 'wc-wba').'</td></tr>';
                    echo apply_filters('the_content', get_option('wba_buy_again_description'));
                    foreach( $order_ids as $order_id ) {
                        $order = wc_get_order($order_id);
                         if( !empty($order) ) {
                             foreach ( $order->get_items() as $item_id => $item ) {
                               $product_id = $item->get_product_id();
                               $variation_id = $item->get_variation_id();
                               if( $variation_id != 0 ) {
                                $product_id = $variation_id;
                               }
                               $product = $item->get_product();
                               if ( has_post_thumbnail($product_id) ) {
                                    $src = wp_get_attachment_image_src( get_post_thumbnail_id($product_id));
                                    $src = $src[0];
                                }else {
                                    $src = wc_placeholder_img_src();
                                }
                               echo '<tr><td><img src="'.esc_url($src).'" width="100" height="100" /></td><td><a href="'.esc_url(get_the_permalink($product_id)).'">'.esc_html($product->get_name()).'</a></td><td>'.wp_kses_post($product->get_price_html()).'</td><td><input type="number" class="wba_item_qty" name="quantity" value="1" min="1" /></td><td><button class="button wba_add_to_cart" data-product_id="'.esc_attr($product_id).'">'.esc_html__('Add to Cart', 'wc-wba').'</button></td></tr>';
                            }
                        }
                    }
                    echo '</table>';
                }else{
                    esc_html_e('No product bought', 'wc-wba');
                }
            });
            flush_rewrite_rules();
        }
        public function wba_myaccount_page_endpoints( $items ) {
            unset($items['customer-logout']);
            $items['buy_again'] = !empty(get_option('wba_buy_again_endpoint_title')) ? get_option('wba_buy_again_endpoint_title') : esc_html__('Buy Again', 'wc-wba');
            $items['customer-logout'] = esc_html__('Logout', 'wc-wba');
            return $items;
        }
    }
    new WBA_Buy_Again_Frontend();
}