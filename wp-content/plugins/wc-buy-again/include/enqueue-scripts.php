<?php
if ( !defined('ABSPATH') ) {
    exit;
}
if ( !class_exists('WBA_Buy_Again_Enqueue') ) {
	class WBA_Buy_Again_Enqueue {
		public function __construct() {
			add_action( 'wp_enqueue_scripts', array($this,'wba_frontend_enqueue') );
		}
		public function wba_frontend_enqueue() {
			$description = !empty(get_option('wba_buy_again_information')) ? get_option('wba_buy_again_information') : esc_html__('You have bought this product already.', 'wc-wba');
			wp_enqueue_script( 'wba-frontend-script', WP_BA_URL.'assets/frontend-script.js', array('jquery'));
			wp_localize_script('wba-frontend-script', 'wba_add_to_cart', array(
				'ajaxurl' => admin_url('admin-ajax.php'),
				'loading' => esc_html__('Adding...', 'wc-wba'),
				'notice'  => wp_kses_post( $description . '<a href="'. esc_url(home_url('my-account/buy_again')) . '"> '.esc_html__('View', 'wc-wba') . '</a>', 'wc-wba'),
				'user_id' => esc_attr(get_current_user_id()),
				)
			);
		}
	}
	new WBA_Buy_Again_Enqueue();
}