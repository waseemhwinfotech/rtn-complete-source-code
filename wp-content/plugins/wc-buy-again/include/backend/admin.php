<?php
if ( !defined( 'ABSPATH' ) ) {
    exit;
}
if ( !class_exists('WBA_Buy_Again_Admin') ) {
	class WBA_Buy_Again_Admin {
		public function __construct() {
			add_action( 'admin_menu', array($this, 'wba_page_menu') );
			add_action('admin_init', array($this, 'wba_page_settings'));
		}
		public function wba_page_menu() {
			add_menu_page( esc_html__('Buy Again', 'wc-wba'), esc_html__('Buy Again', 'wc-wba'), 'manage_options', 'wba-buy-again', array( $this, 'wba_settings_page' ), 'dashicons-controls-repeat', 57 );
			add_submenu_page( 'wba-buy-again', esc_html__('Settings', 'wc-wba'), esc_html__('Settings', 'wc-wba'), 'manage_options', 'wba-buy-again', array( $this, 'wba_settings_page') );
		}
		public function wba_settings_page() {
			require_once WP_BA_DIR.'include/backend/settings.php';
		}
		public function wba_page_settings(){
			register_setting('wba_general_settings', 'wba_buy_again_enable_module');
			register_setting('wba_general_settings', 'wba_buy_again_endpoint_title');
			register_setting('wba_general_settings', 'wba_buy_again_description');
			register_setting('wba_general_settings', 'wba_buy_again_order_button_label');
			register_setting('wba_general_settings', 'wba_buy_again_information');
		}
	}
	new WBA_Buy_Again_Admin();
}