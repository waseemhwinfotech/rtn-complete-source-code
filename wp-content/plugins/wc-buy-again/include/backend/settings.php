<?php
if ( !defined('ABSPATH') ) {
    exit; 
}
if( !class_exists('WBA_Buy_Again_Settings') ) {
	class WBA_Buy_Again_Settings {
		public function __construct() {
			$current = isset( $_GET['tab']) ? $_GET['tab'] : 'general-settings';
	        $tabs = array(
			'general-settings' => esc_html__('General Settings', 'wc-wba'),
	        );
	        $this->wba_init_tabs( apply_filters('wba_setting_tabs', $tabs) );
	        $this->wba_current_tab( apply_filters('wba_current_setting_tab', $current) );
		}
		public function wba_init_tabs( $tabs=array() ) {
	        $current = isset($_GET['tab']) ? $_GET['tab'] : 'general-settings';
	        $html='<h5 class="nav-tab-wrapper">';
	        foreach( $tabs as $tab => $name ) {
	        $class = ($tab == $current) ? 'nav-tab-active' : '';
	        $html .= '<a class="nav-tab '.esc_attr($class).'" href="'.admin_url().'admin.php?page=wba-buy-again&tab='.esc_attr($tab).'">'.esc_html($name).'</a>';
	        }
	        $html.='</h5>';
	        echo wp_kses_post($html);
	    }
	    public function wba_current_tab( $current='general-settings' ) {
	        switch( $current ) {
				case 'general-settings':
					$this->wba_general_settings();
					break;
	        }
		}
		public function wba_general_settings(){ ?>
			<form method="post" action="options.php">
				<?php settings_errors();
				settings_fields('wba_general_settings');
				do_settings_sections('wba_general_settings'); ?>
				<h3><?php esc_html_e('Basic Settings of Buy Again', 'wc-wba'); ?></h3>
				<table class="form-table">
					<tr>
						<?php $value = get_option('wba_buy_again_enable_module'); ?>
						<th><label for="wba_buy_again_enable_module"><?php esc_html_e('Enable Module','wc-wba'); ?></label></th>
						<td>
							<label><input type="checkbox" name="wba_buy_again_enable_module" id="wba_buy_again_enable_module" value="yes" <?php checked($value,'yes'); ?> />
							<span><?php esc_html_e('You can enable/disable plugin functionality here', 'wc-wba'); ?></span><label>
						</td>
					</tr>
					<tr>
						<?php $value = get_option('wba_buy_again_endpoint_title'); ?>
						<th><label for="wba_buy_again_endpoint_title"><?php esc_html_e('Tab Title','wc-wba'); ?></label></th>
						<td>
							<label><input type="text" name="wba_buy_again_endpoint_title" id="wba_buy_again_endpoint_title" class="regular-text" value="<?php echo esc_attr($value); ?>" />
						</td>
					</tr>
					<tr>
						<?php $value = get_option('wba_buy_again_information'); ?>
						<th><label for="wba_buy_again_information"><?php esc_html_e('Buy Again Description','wc-wba'); ?></label></th>
						<td>
							<label><input type="text" name="wba_buy_again_information" id="wba_buy_again_information" class="regular-text" value="<?php echo esc_attr($value); ?>" />
						</td>
					</tr>
					<tr>
						<?php $value = get_option('wba_buy_again_order_button_label'); ?>
						<th><label for="wba_buy_again_order_button_label"><?php esc_html_e('Order Details Button Label','wc-wba'); ?></label></th>
						<td>
							<label><input type="text" name="wba_buy_again_order_button_label" id="wba_buy_again_order_button_label" class="regular-text" value="<?php echo esc_attr($value); ?>" />
						</td>
					</tr>
					<tr>
						<th><label for="wba_buy_again_description"><?php esc_html_e('Description Before Products', 'wc-wba'); ?></label></th>
						<td>
						<?php $value = html_entity_decode( get_option('wba_buy_again_description') );
						echo wp_editor( $value, 'wba_buy_again_description', array( 'textarea_rows' => 3, 'editor_height' => 350, ) ); ?>
						</td>
					</tr>
				</table>
				<?php submit_button(); ?>
			</form>
			<?php
		}
	}
	new WBA_Buy_Again_Settings();
}