( function( $ ) {
'use strict';
    jQuery( document ).ready( function() {
        jQuery(document).on('click', '.wba_add_to_cart', function( e ) {
            e.preventDefault();
            var btn_this = jQuery(this);
            var btn_text = btn_this.text();
            btn_this.text(wba_add_to_cart.loading);
            var product_id = jQuery(this).attr('data-product_id');
            var qty = jQuery(this).closest('tr').find('input.wba_item_qty').val();
            var data = {
                'action'     : 'wba_add_to_cart_item',
                'product_id' : product_id,
                'qty'        : qty
            };
            jQuery.post(wba_add_to_cart.ajaxurl, data, function( response ) {
                if( response.success ) {
                    btn_this.text(btn_text);
                }
            });
        });
        jQuery( '.variations_form' ).on( 'woocommerce_variation_select_change', function() {
            jQuery('.woocommerce-notices-wrapper').html('');
        });
        jQuery( document ).one( 'click', '.product-type-variable', function() {
        });
        jQuery( '.single_variation_wrap' ).on( 'show_variation', function( event, variation ) {
            jQuery( 'a.wcpai_chart_shop_button' ).attr( 'data-product_id', variation.variation_id );
            var data = {
                'action'       : 'wba_check_sold_item',
                'variation_id' : variation.variation_id,
                'user_id'      : wba_add_to_cart.user_id,
            };
            jQuery.post( wba_add_to_cart.ajaxurl, data, function( response ) {
                if( response.success ) {
                   jQuery('.woocommerce-notices-wrapper').html('<div class="woocommerce-info">'+wba_add_to_cart.notice+"</div>");
                }
            });
        });
    });
})(jQuery);