== Buy Again For WooCommerce == 

Buy Again for WooCommerce allows customers to repurchase the products quickly. It helps them to rebuy the products that they purchased in the past from your shop. They can order again for all the products which they have bought previously.

When customers purchase products that list in a table in the My Account page of the Buy Again section, customers can add multiple products to the cart, purchase a product directly, or use the Buy Again button in the Buy Again section.

== Key Features ==
Supports product variations.
Allows for custom labels and description texts.
Helps to re-purchase products saved at My Account page
Allows to re-purchase products saved at my Account Orders
Easy to configure and customize.

== Changelog ==

Version 1.0.0
* Initial release.