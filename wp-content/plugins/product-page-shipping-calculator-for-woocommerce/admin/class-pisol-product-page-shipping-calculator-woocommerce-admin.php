<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       piwebsolution.com
 * @since      1.0.0
 *
 * @package    Pisol_Product_Page_Shipping_Calculator_Woocommerce
 * @subpackage Pisol_Product_Page_Shipping_Calculator_Woocommerce/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Pisol_Product_Page_Shipping_Calculator_Woocommerce
 * @subpackage Pisol_Product_Page_Shipping_Calculator_Woocommerce/admin
 * @author     PI Websolution <sales@piwebsolution.com>
 */
class Pisol_Product_Page_Shipping_Calculator_Woocommerce_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		add_action('wp_loaded', array($this,'register_menu'));
		
	}

	public function register_menu(){
		if(is_admin() && ! ( defined( 'DOING_AJAX' ) && DOING_AJAX )){
			new pisol_ppscw_menu( $this->plugin_name, $this->version );
			
			new pisol_ppscw_basic_option($this->plugin_name);
			new pisol_ppscw_design_setting($this->plugin_name);
			if(pisol_ppscw_estimate_pro_present()){
				new pisol_ppscw_estimate_setting($this->plugin_name);
			}else{
				new pisol_ppscw_free_estimate_setting($this->plugin_name);
			}
			new pisol_ppscw_other_plugins($this->plugin_name);
			new pisol_ppscw_warning_messages();
		}
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Pisol_Product_Page_Shipping_Calculator_Woocommerce_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Pisol_Product_Page_Shipping_Calculator_Woocommerce_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Pisol_Product_Page_Shipping_Calculator_Woocommerce_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Pisol_Product_Page_Shipping_Calculator_Woocommerce_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/pisol-product-page-shipping-calculator-woocommerce-admin.js', array( 'jquery' ), $this->version, false );

	}

}
