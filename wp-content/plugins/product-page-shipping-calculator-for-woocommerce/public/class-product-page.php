<?php

class pisol_ppscw_product_page_calculator{

    function __construct(){

		$position = apply_filters('pi_ppscw_cal_position_filter', get_option('pi_ppscw_calc_position', 'woocommerce_after_add_to_cart_form'));

        add_action( $position, array(__CLASS__, 'calculator'));

		add_action('wp_ajax_pisol_cal_shipping', array(__CLASS__, 'applyShipping') );
        add_action('wp_ajax_nopriv_pisol_cal_shipping', array(__CLASS__, 'applyShipping') );
		add_filter( 'woocommerce_notice_types',array(__CLASS__, 'onlyPassErrorNotice') );
		
		add_filter('pi_ppscw_hide_calculator_on_single_product_page',array(__CLASS__, 'hideCalculator'),10,2 );

		$result_position = apply_filters('pi_ppscw_result_positon', get_option('pi_ppscw_result_position','pi_ppscw_before_calculate_button'));
		add_action($result_position, array(__CLASS__, 'resultHtml'));
    }

	static function resultHtml(){
		echo '<div id="pisol-ppscw-alert-container"></div>';
	}

    static function calculator(){
		global $product;

		if(apply_filters('pi_ppscw_hide_calculator_on_single_product_page',false, $product)){
			return;
		}

		if(is_object($product)){
			$product_id = $product->get_id();
		}else{
			$product = "";
		}

		$button_text = get_option('pi_ppscw_open_drawer_button_text','Select delivery location');
		$update_address_btn_text = get_option('pi_ppscw_update_button_text','Update Address');

		include 'partials/shipping-calculator.php';
	}

	static function hideCalculator($val, $product){
		if(is_object($product) && $product->is_virtual()) return true;

		return $val;
	}

	static function applyShipping(){
		if ( self::doingCalculation() ){

			if(isset($_POST['action_auto_load']) && self::disableAutoLoadEstimate()){
				$return['shipping_methods'] = sprintf('<div class="pisol-ppscw-alert">%s</div>', get_option('pi_ppscw_no_address_added_yet', 'Insert your location to get the shipping method'));
				wp_send_json($return);
			}

			$return = array();
			WC_Shortcode_Cart::calculate_shipping();
			WC()->cart->calculate_totals();


			$item_key = self::addTestProductForProperShippingCost();
			

			if(WC()->cart->get_cart_contents_count() == 0 ){
				$blank_package = self::get_shipping_packages();
				WC()->shipping()->calculate_shipping($blank_package );
			}
			$packages = WC()->shipping()->get_packages();
			$shipping_methods = self::getShippingMethods($packages);
			$return['error'] = wc_print_notices(true);
			//error_log(print_r($return,true));
			//wc_clear_notices();
			$return['shipping_methods'] = self::messageTemplate($shipping_methods);
			echo json_encode($return);

			if($item_key){
				WC()->cart->remove_cart_item($item_key);
			}
		}
		die;
	}

	static function noShippingLocationInserted(){
		$country = WC()->customer->get_shipping_country();
		if(empty($country) || $country == 'default') return true;

		return false;
	}

	static function onlyPassErrorNotice($notice_type){
		if ( self::doingCalculation() ){
			return array('error');
		}
		return $notice_type;
	}

	static function doingCalculation(){
		if ( ! empty( $_POST['calc_shipping'] )  && isset($_REQUEST['pisol-woocommerce-shipping-calculator-nonce'])){
			return true;
		}
		return false;
	}

	static function addTestProductForProperShippingCost(){
			$product_id = filter_input(INPUT_POST, 'product_id');
			$quantity = filter_input(INPUT_POST, 'quantity');
			if(empty($quantity)) $quantity = 1;

			if($product_id){
				$variation_id = filter_input(INPUT_POST, 'variation_id');
				if(!$variation_id){
					$variation_id = 0;
				}
				$item_key = self::addProductToCart($product_id, $variation_id, $quantity);
			}else{
				$item_key = "";
			}
			return $item_key;
	}

	static function addProductToCart($product_id, $variation_id, $quantity = 1){
		$consider_product_quantity = apply_filters('pisol_ppscw_consider_quantity_in_shipping_calculation', get_option('pi_ppscw_consider_quantity_field', 'dont-consider-quantity-field'), $product_id, $variation_id, $quantity);

		if($consider_product_quantity == 'dont-consider-quantity-field'){
			if(self::productExistInCart( $product_id, $variation_id )) return "";
			$quantity = 1;
		}
		
		$item_key = WC()->cart->add_to_cart(
			$product_id,
			$quantity,
			$variation_id,
			array(),
			array(
				'pisol_test_product_for_calculation'   => '1',
			)
		);
		return $item_key;
	}

	static function productExistInCart( $product_id, $variation_id ) {
		if ( ! WC()->cart->is_empty() ) {
			foreach(WC()->cart->get_cart() as $cart_item ) {
				if( $cart_item['product_id'] == $product_id && $cart_item['variation_id'] == $variation_id){
					return true;
				}
			}
		}
		return false; 
	}

	static function get_shipping_packages() {
		return array(
				array(
					'contents'        => array(),
					'contents_cost'   => 0,
					'applied_coupons' => '',
					'user'            => array(
						'ID' => get_current_user_id(),
					),
					'destination'     => array(
						'country'   => self::get_customer()->get_shipping_country(),
						'state'     => self::get_customer()->get_shipping_state(),
						'postcode'  => self::get_customer()->get_shipping_postcode(),
						'city'      => self::get_customer()->get_shipping_city(),
						
					),
					'cart_subtotal'   => 0,
				),
			);
	
	}

	static function get_customer() {
		return WC()->customer;
	}


	static function getShippingMethods($packages){
		$shipping_methods = array();
		$product_id = filter_input(INPUT_POST, 'product_id');
		$variation_id = filter_input(INPUT_POST, 'variation_id');
		foreach($packages as $package){
			if(empty($package['rates']) || !is_array($package['rates'])) break;

			foreach($package['rates'] as $id => $rate){
				$title = wc_cart_totals_shipping_method_label($rate);
				$shipping_methods[$id] = apply_filters('pisol_ppscw_shipping_method_name',$title, $rate, $product_id, $variation_id);
			}
		}
		return $shipping_methods;
	}

	static function noMethodAvailableMsg(){

		if(self::noShippingLocationInserted()){
			return get_option('pi_ppscw_no_address_added_yet', 'Insert your location to get the shipping method');
		}else{
			return get_option('pi_ppscw_no_shipping_methods_msg', 'No shipping Method available for your area');
		}
	}

	static function disableAutoLoadEstimate(){
		$auto_loading = get_option('pi_ppscw_auto_calculation', 'enabled');

		if($auto_loading == 'enabled') return false;

		return true;
	}

	static function messageTemplate($shipping_methods){

		

		$message_above = get_option('pi_ppscw_above_shipping_methods', 'Following shipping methods are available for your area:');

		if(is_array($shipping_methods) && !empty($shipping_methods)){
			$html = '';
			foreach($shipping_methods as $id=> $method){
				$html .= sprintf('<li id="%s">%s</li>',esc_attr($id), $method);
			}
			if(!empty($html)){
				$shipping_methods_msg = '<ul class="pisol-ppscw-methods">'.$html.'</ul>';
			}else{
				$shipping_methods_msg = "";
			}
		}else{
			$shipping_methods_msg = "";
		}

		$when_shipping_msg = $message_above.'<br>'.$shipping_methods_msg;

		$msg = is_array($shipping_methods) && !empty($shipping_methods) ? $when_shipping_msg : self::noMethodAvailableMsg();

		return sprintf('<div class="pisol-ppscw-alert">%s</div>', $msg);
	}
}

new pisol_ppscw_product_page_calculator();