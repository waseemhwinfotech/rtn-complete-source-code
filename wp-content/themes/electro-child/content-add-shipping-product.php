<?php
/**
 * The template for displaying the shiping page.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: Shipping Manager
 *
 * @package electro-child
 */

remove_action( 'electro_content_top', 'electro_breadcrumb', 10 );

do_action( 'electro_before_homepage_v1' );

$home_v1 		= electro_get_home_v1_meta();
$header_style 	= isset( $home_v1['hpc']['header_style'] ) ? $home_v1['hpc']['header_style'] : 'v1';
electro_get_header( $header_style ); ?>

<div class="wpc-condition-group-wrap">
   <p class="or-text"><strong>Or</strong></p>
   <div class="wpc-condition-group clearfix repeater-active" data-group="0">
      <p class="match-text">Match all of the following rules to allow free shipping:</p>
      <div class="wpc-conditions-list">
         <div class="wpc-condition-wrap">
            <!-- Condition -->
            <span class="wpc-condition-field-wrap">
               <select name="conditions[0][condition]" id="" class="input-select wpc-condition">
                  <optgroup label="Cart">
                     <option value="subtotal" selected="selected">Subtotal</option>
                     <option value="subtotal_ex_tax">Subtotal ex. taxes</option>
                     <option value="tax">Tax</option>
                     <option value="quantity">Quantity</option>
                     <option value="contains_product">Contains product</option>
                     <option value="coupon">Coupon</option>
                     <option value="weight">Weight</option>
                     <option value="contains_shipping_class">Contains shipping class</option>
                  </optgroup>
                  <optgroup label="User Details">
                     <option value="zipcode">Zipcode</option>
                     <option value="city">City</option>
                     <option value="state">State</option>
                     <option value="country">Country</option>
                     <option value="role">User role</option>
                  </optgroup>
                  <optgroup label="Product">
                     <option value="width">Width</option>
                     <option value="height">Height</option>
                     <option value="length">Length</option>
                     <option value="stock">Stock</option>
                     <option value="stock_status">Stock status</option>
                     <option value="category">Category</option>
                  </optgroup>
               </select>
            </span>
            <!-- Operator -->
            <span class="wpc-operator-field-wrap">
               <select name="conditions[0][operator]" id="" class="input-select wpc-operator">
                  <option value="==">Equal to</option>
                  <option value="!=">Not equal to</option>
                  <option value=">=">Greater or equal to</option>
                  <option value="<=">Less or equal to </option>
               </select>
            </span>
            <!-- Value -->
            <span class="wpc-value-field-wrap"><input name="conditions[0][961485506][value]" type="text" id="" value="" class="input-text wpc-value" placeholder=""></span>
            <!-- Delete-->
            <a class="button wpc-condition-delete" href="javascript:void(0);"></a><span class="wpc-description">
            <span class="woocommerce-help-tip"></span>
            </span>
         </div>
      </div>
      <div class="wpc-condition-template hidden" style="display: none;">
         <div class="wpc-condition-wrap">
            <!-- Condition -->
            <span class="wpc-condition-field-wrap">
               <select name="conditions[0][9999][condition]" id="" class="input-select wpc-condition" data-id="9999">
                  <optgroup label="Cart">
                     <option value="subtotal" selected="selected">Subtotal</option>
                     <option value="subtotal_ex_tax">Subtotal ex. taxes</option>
                     <option value="tax">Tax</option>
                     <option value="quantity">Quantity</option>
                     <option value="contains_product">Contains product</option>
                     <option value="coupon">Coupon</option>
                     <option value="weight">Weight</option>
                     <option value="contains_shipping_class">Contains shipping class</option>
                  </optgroup>
                  <optgroup label="User Details">
                     <option value="zipcode">Zipcode</option>
                     <option value="city">City</option>
                     <option value="state">State</option>
                     <option value="country">Country</option>
                     <option value="role">User role</option>
                  </optgroup>
                  <optgroup label="Product">
                     <option value="width">Width</option>
                     <option value="height">Height</option>
                     <option value="length">Length</option>
                     <option value="stock">Stock</option>
                     <option value="stock_status">Stock status</option>
                     <option value="category">Category</option>
                  </optgroup>
               </select>
            </span>
            <!-- Operator -->
            <span class="wpc-operator-field-wrap">
               <select name="conditions[0][9999][operator]" id="" class="input-select wpc-operator">
                  <option value="==">Equal to</option>
                  <option value="!=">Not equal to</option>
                  <option value=">=">Greater or equal to</option>
                  <option value="<=">Less or equal to </option>
               </select>
            </span>
            <!-- Value -->
            <span class="wpc-value-field-wrap"><input name="conditions[0][9999][value]" type="text" id="" value="" class="input-text wpc-value" placeholder=""></span>
            <!-- Delete-->
            <a class="button wpc-condition-delete" href="javascript:void(0);"></a><span class="wpc-description">
            <span class="woocommerce-help-tip"></span>
            </span>
         </div>
      </div>
      <a style="margin-top: 0.5em; margin-right: 63px;" class="wpc-condition-add wpc-add button alignright" data-group="0" href="javascript:void(0);">Add condition</a>
   </div>
</div>
<?php 

get_footer();