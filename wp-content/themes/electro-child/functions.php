<?php
/**
 * Electro Child
 *
 * @package electro-child
 */

/**
 * Include all your custom code here
 */
if( !function_exists('yith_woocompare_set_new_text_for_added_label') ){
    function yith_woocompare_set_new_text_for_added_label( $label ){
        $label = "Compare";
        return $label;        
    }
    add_filter('yith_woocompare_compare_added_label','yith_woocompare_set_new_text_for_added_label');
}
function custom_theme_setup() {
    add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'custom_theme_setup' );
//90202
// To change add to cart text on single product page
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woocommerce_custom_single_add_to_cart_text' ); 
function woocommerce_custom_single_add_to_cart_text() {
    return __( 'Add to Cart', 'woocommerce' ); 
}

function wpb_adding_scripts() {
      wp_register_script('location-zip', get_template_directory_uri() . '/location.js', array('jquery-core-js'),'1.1', true);
     wp_enqueue_script('location-zip');
 } 

add_action( 'wp_enqueue_scripts', 'wpb_adding_scripts' ); 

function locationtrack(){
?>
<style>
    .pisol-shipping-calculator-form input[type="text"], .pisol-shipping-calculator-form select
    {
        padding: 12px !important;
    }
    #calc_shipping_postcode_field {
width: 50%;
margin-right: 10px;
}
#calc_shipping_state_field,#calc_shipping_city_field,#calc_shipping_country_field,.pisol-shipping-calculator-button {
margin: 0px !important;display: none !important;
}
.button.pisol-shipping-calculator-button, {

display: none !important;
}
.pisol-shipping-calculator-form
{
    padding: 0px !important;
    border: none !important;
}

</style>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDvS94lLUPVLMnZzflNjE5zSOuOiEKGEV8"
  type="text/javascript"></script>
<script>
jQuery(document).ready(function($) {
    $(".pisol-shipping-calculator-form").show()
    $(".pisol-update-address-button").text("Check Availability")
    $(".pisol-update-address-button").click(function(e){
        e.preventDefault();

            const geocoder = new google.maps.Geocoder();
            geocoder.geocode({
              componentRestrictions: {
                country: 'US',
                postalCode: $("#calc_shipping_postcode").val()
              }
            }, function(results, status) {
              if (status == 'OK') {
                console.log(results[0].formatted_address)
                var formatted_address = results[0].formatted_address;
                var splitted = formatted_address.split(",")
                var getState = splitted[1].trim().split(" ")
                $("#calc_shipping_city").val(splitted[0]);
                $("#calc_shipping_state").val(getState);
                $(".pisol-woocommerce-shipping-calculator").submit()
              } else {
                window.alert('Geocode was not successful for the following reason: ' + status);
              }
            });
    })
});
</script>
<?php
}
add_action( 'wp_footer', 'locationtrack' );

add_action('template_redirect','check_if_logged_in');
function check_if_logged_in()
{
    $pageid = 3855; // your checkout page id
    $pageid2=10539;//offer page
    $pageid3=10544;//notifications apge
    $pageid4=3135;
    if(!is_user_logged_in() && is_page($pageid))
    {
        $url = add_query_arg(
            'redirect_to',
            get_permalink($pagid),
            site_url('/login/') // your my acount url
        );
        wp_redirect($url);
        exit;
    }
	elseif(!is_user_logged_in() && is_page($pageid2))
	{
		$url = add_query_arg(
            'redirect_to',
            get_permalink($pagid),
            site_url('/login/') // your my acount url
        );
        wp_redirect($url);
        exit;
	}
	
	elseif(!is_user_logged_in() && is_page($pageid3))
	{
		$url = add_query_arg(
            'redirect_to',
            get_permalink($pagid),
            site_url('/login/') // your my acount url
        );
        wp_redirect($url);
        exit;
	}
	
	elseif(!is_user_logged_in() && is_page($pageid4))
	{
		$url = add_query_arg(
            'redirect_to',
            get_permalink($pagid),
            site_url('/login/') // your my acount url
        );
        wp_redirect($url);
        exit;
	}
}
add_action( 'wp_head', 'wps_params', 10 );
function wps_params() {
    ?>
    
	<script>
// 	if (window.location.pathname == '/' && jQuery(window).width() <= 767) {
// 	   window.location = "/m-v/";
// 	}
	</script>

    <?php
}
